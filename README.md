# Register Form PHP
## Day07 Assignment
- [x] Add reset button
- [x] Save user input to session

## Project structure
```
[drwxrwxr-x]  .
├── [-rwxrwxrwx]  confirm.php
├── [drwxrwxr-x]  css
│   └── [-rw-rw-r--]  styles.css
├── [-rw-rw-r--]  index.php
├── [-rw-rw-r--]  README.md
└── [-rw-rw-r--]  register.php

1 directory, 5 files
```
