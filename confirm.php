<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Confirm</title>
    <link rel="stylesheet" href="./css/styles.css">
</head>
<body>
    <form class="container">
        <div class="row">
            <div class="col1 label required">Họ và tên</div>
            <div class="col2">
            <?php session_start(); $name = $_SESSION["name"]; echo "<div style=\"margin: auto 0;\">$name</div>" ?>
            </div>
        </div>
        <div class="row">
            <div class="col1 label required">Giới tính</div>
            <div class="col2">
            <?php session_start(); $gender = $_SESSION["gender"]; echo "<div style=\"margin: auto 0;\">$gender</div>" ?>
            </div>
        </div>
        <div class="row">
            <div class="col1 label required">Phân khoa</div>
            <div class="col2">
            <?php session_start(); $department = $_SESSION["department"]; echo "<div style=\"margin: auto 0;\">$department</div>" ?>
            </div>
        </div>
        <div class="row">
            <div class="col1 label required">Ngày sinh</div>
            <div class="col2">
            <?php session_start(); $birthday = $_SESSION["birthday"]; echo "<div style=\"margin: auto 0;\">$birthday</div>" ?>
            </div>
        </div>
        <div class="row">
            <div class="col1 label">Địa chỉ</div>
            <div class="col2">
            <?php session_start(); $address = $_SESSION["address"]; echo "<div style=\"margin: auto 0;\">$address</div>" ?>
            </div>
        </div>
        <div class="row">
            <div class="col1 label">Hình ảnh</div>
            <div class="col2">
            <?php session_start(); $image = $_SESSION["img"]; if ($image !== "") {echo "<img src=\"$image\" alt=\"\" style=\"width: 100px; height: 100px;\">";} ?>
            </div>
        </div>
        <div class="row" style="justify-content: center; margin: 25px 0 0 0;">
            <button style="padding: 16px 56px;">Xác nhận</button>
        </div>
    </form>
</body>

</html>